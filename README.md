# Flash message control #

A visual component for applications using the [Nette](https://nette.org/) framework, allowing them to work easily
and anywhere in a unified way with [flash messages](https://doc.nette.org/en/application/components#toc-flash-messages).

## Requirements ##

- [PHP](https://php.net/) >= 8.3

## Installation ##

The best way to install [**interitty/flash-message-control**](https://gitlab.com/interitty/flash-message-control) is using [Composer](https://getcomposer.org/):

```bash
composer require interitty/flash-message-control
```

### Register in `config.neon` ###
Then register the component in the [Nette config](https://doc.nette.org/en/3.1/bootstrap#toc-di-container-configuration) file:

```neon
# app/config/config.neon
services:
    FlashMessageControlFactory:
        implement: Interitty\FlashMessageControl\FlashMessageControlFactoryInterface
```

### Register in Presenter or Control ###

Any component or presenter that needs to work with a [flash messages](https://doc.nette.org/en/application/components#toc-flash-messages) can then add this component.

```php
<?php

declare(strict_types=1);

namespace App\Presenters;

use Interitty\FlashMessageControl\FlashMessageControl;
use Interitty\FlashMessageControl\FlashMessageControlFactoryInterface;
use Interitty\FlashMessageControl\FlashMessageControlHelperTrait;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
    use FlashMessageControlHelperTrait;

    protected FlashMessageControlFactoryInterface $flashMessageControlFactory;

    public function injectFlashMessageControlFactory(FlashMessageControlFactoryInterface $flashMessageControlFactory): void
    {
        $this->flashMessageControlFactory = $flashMessageControlFactory;
    }

    protected function createComponentFlashMessage(string $name = 'flashMessage'): FlashMessageControl
    {
        $component = $this->flashMessageControlFactory->create($this, $name);
        return $component;
    }
}
```

> ℹ️ Simplified registration
>
> If the extension [`interitty/component-model`](https://gitlab.com/interitty/component-model) is used in the application,
> the registration in Presenter is much easier.
>
> ```php
><?php
>
>declare(strict_types=1);
>
>namespace App\Presenters;
>
>use Interitty\ComponentModel\ComponentLocatorTrait;
>use Interitty\FlashMessageControl\FlashMessageControlHelperTrait;
>use Nette\Application\UI\Presenter;
>
>abstract class BasePresenter extends Presenter
>{
>    use ComponentLocatorTrait;
>    use FlashMessageControlHelperTrait;
>}
>```

### Register in latte template ###

Once the component is registered, you can easily render it within the [latte](https://latte.nette.org/) template.

```latte
{control flashMessage}
```

## Usage ##

Thanks to the used `FlashMessageControlHelperTrait` auxiliary functions are available for adding a flash message of the appropriate type.

```php
<?php

declare(strict_types=1);

namespace App\Presenters;

use Interitty\FlashMessageControl\FlashMessageControlFactory;
use Interitty\FlashMessageControl\FlashMessageControlHelperTrait;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
    use FlashMessageControlHelperTrait;

    public function renderDefault()
    {
        $this->flashMessageSuccess('The :control was succesfully registered.', ['control' => FlashMessageControlFactory::class]);
    }
}
```

It is also possible to use the otherwise standard [`flashMessage` method](https://doc.nette.org/en/application/components#toc-flash-messages),
which has been extended with a third parameter to pass additional parameters that will be inserted after [translation](https://doc.nette.org/en/best-practices/translations)
in place of their `:placeholders`.

All these methods return an `Interitty\FlashMessageControl\FlashMessage` object that has a fluent interface for possible further settings.

```php
<?php

declare(strict_types=1);

namespace App\Presenters;

use Interitty\FlashMessageControl\FlashMessage;
use Interitty\FlashMessageControl\FlashMessageControlHelperTrait;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
    use FlashMessageControlHelperTrait;

    public function renderDefault()
    {
        $this->flashMessage()
            ->setMessage('The Interitty flash messages are :status!')
            ->setType(FlashMessage::TYPE_SUCCESS)
            ->addData('status', 'awesome');
    }
}
```

### Custom template ###

By default, the component uses a template that renders individual flash messages as separate `div` blocks with the appropriate css class settings.

However, it can certainly be useful to set a custom rendering method. This can easily be done by setting the path to the custom template,
for example directly in the Nette config file.

```neon
# app/config/config.neon
services:
    FlashMessageControlFactory:
        implement: Interitty\FlashMessageControl\FlashMessageControlFactoryInterface
        setup:
            - setTemplateFile(%appDir%/Controls/FlashMessage/FlashMessageControlCustom.latte)
```
