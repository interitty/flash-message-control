<?php

declare(strict_types=1);

namespace Interitty\FlashMessageControl;

use Interitty\Exceptions\Exceptions;
use Interitty\Utils\Strings;
use Nette\Localization\Translator;
use Nette\MemberAccessException;
use stdClass;
use Stringable;

use function is_scalar;

/**
 * @property-read string $message
 * @property-read string $type
 */
class FlashMessage extends stdClass
{
    /** All available flash message type constants */
    public const string TYPE_ERROR = 'error';
    public const string TYPE_INFO = 'info';
    public const string TYPE_SUCCESS = 'success';
    public const string TYPE_WARNING = 'warning';

    /** @phpstan-var array<string, mixed> */
    protected array $data = [];

    /** @var Translator|null */
    protected ?Translator $translator = null;

    /** @var string */
    protected string $message;

    /** @var string */
    protected string $type;

    /**
     * Constructor
     *
     * @param string $message
     * @param string $type [OPTIONAL] One of FlashMessage::TYPE_* constants
     * @phpstan-param array<string, mixed> $data [OPTIONAL]
     * @return void
     */
    public function __construct(string $message, string $type = self::TYPE_INFO, array $data = [])
    {
        $this->setMessage($message);
        $this->setType($type);
        $this->setData($data);
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Property access helper
     *
     * @param string $name
     * @return string
     * @throws MemberAccessException
     */
    public function __get($name): string
    {
        if ($name === 'message') {
            $value = $this->toString();
        } elseif ($name === 'type') {
            $value = $this->getType();
        } else {
            throw Exceptions::extend(MemberAccessException::class)
                    ->setMessage('Cannot read an undeclared property :class:::name')
                    ->addData('class', self::class)
                    ->addData('name', $name);
        }
        return $value;
    }

    /**
     * Serialization helper
     *
     * @phpstan-return array{'message': string, 'type': string}
     */
    public function __serialize(): array
    {
        $data = [
            'message' => $this->toString(),
            'type' => $this->getType(),
        ];
        return $data;
    }

    /**
     * Convert exception to string magic method
     *
     * @return string
     */
    public function __toString(): string
    {
        $data = $this->getData();
        $message = $this->getMessage();
        $translator = $this->getTranslator();
        if ($translator instanceof Translator) {
            $message = $translator->translate($message, $data);
        }
        foreach ($data as $key => $value) {
            if ((is_scalar($value) === true) || ($value instanceof Stringable)) {
                $message = Strings::replace((string) $message, '~:' . $key . '~', (string) $value);
            }
        }
        return (string) $message;
    }

    /**
     * Convert flash message to string helper
     *
     * @return string
     */
    public function toString(): string
    {
        return (string) $this;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Data adder
     *
     * @param string $key
     * @param mixed $value
     * @return static Provides fluent interface
     */
    public function addData(string $key, mixed $value): static
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * Data getter
     *
     * @phpstan-return array<string, mixed>
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Data setter
     *
     * @phpstan-param array<string, mixed> $data
     * @return static Provides fluent interface
     */
    public function setData(array $data): static
    {
        $this->data = [];
        foreach ($data as $key => $value) {
            $this->addData($key, $value);
        }
        return $this;
    }

    /**
     * Message getter
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Message setter
     *
     * @param string $message
     * @return static Provides fluent interface
     */
    public function setMessage(string $message): static
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Translator getter
     *
     * @return Translator|null
     */
    public function getTranslator(): ?Translator
    {
        return $this->translator;
    }

    /**
     * Translator setter
     *
     * @param Translator|null $translator
     * @return static Provides fluent interface
     */
    public function setTranslator(?Translator $translator): static
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * Type getter
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Type setter
     *
     * @param string $type
     * @return static Provides fluent interface
     */
    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    // </editor-fold>
}
