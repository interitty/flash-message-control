<?php

declare(strict_types=1);

namespace Interitty\FlashMessageControl;

use Interitty\Application\UI\Control;
use Nette\HtmlStringable;
use stdClass;
use Stringable;

use function is_string;

class FlashMessageControl extends Control
{
    /** All available snippet name constants */
    public const string SNIPPET_FLASHES = 'flashes';

    /**
     * Saves the message to template, that can be displayed after redirect
     *
     * @param string|stdClass|FlashMessage|Stringable|null $message [OPTIONAL]
     * @param string $type [OPTIONAL] One of FlashMessage::TYPE_* constant
     * @phpstan-param array<string, mixed> $data [OPTIONAL]
     * @phpstan-return ($message is (string|FlashMessage|Stringable|HtmlStringable) ? FlashMessage : stdClass)
     */
    public function flashMessage($message = null, string $type = FlashMessage::TYPE_INFO, array $data = []): stdClass
    {
        if ($message instanceof FlashMessage) {
            $flashMessage = $this->createFlashMessage($message->getMessage(), $type, $data);
        } elseif ((is_string($message) === true) || ($message instanceof Stringable) || ($message === null)) {
            $flashMessage = $this->createFlashMessage((string) $message, $type, $data);
        } else {
            $flashMessage = $message;
        }

        $flash = parent::flashMessage($flashMessage, $type);
        $this->redrawControl(self::SNIPPET_FLASHES);
        return $flash;
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Flash message factory
     *
     * @param string $message
     * @param string $type One of FlashMessage::TYPE_* constant
     * @phpstan-param array<string, mixed> $data [OPTIONAL]
     * @return FlashMessage
     */
    protected function createFlashMessage(string $message, string $type, array $data = []): FlashMessage
    {
        $translator = $this->getTranslator();
        $flashMessage = new FlashMessage($message, $type, $data);
        $flashMessage->setTranslator($translator);
        return $flashMessage;
    }

    // </editor-fold>
}
