<?php

namespace Interitty\FlashMessageControl;

use Interitty\ComponentModel\ComponentFactoryInterface;
use Nette\ComponentModel\IContainer as ContainerInterface;

interface FlashMessageControlFactoryInterface extends ComponentFactoryInterface
{
    /**
     * FlashMessageControl factory
     *
     * @param ContainerInterface|null $parent [OPTIONAL]
     * @param string|null $name [OPTIONAL]
     * @return FlashMessageControl
     */
    public function create(?ContainerInterface $parent = null, ?string $name = null): FlashMessageControl;
}
