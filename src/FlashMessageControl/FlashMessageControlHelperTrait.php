<?php

declare(strict_types=1);

namespace Interitty\FlashMessageControl;

use Nette\ComponentModel\IComponent as ComponentInterface;
use Nette\HtmlStringable;
use stdClass;
use Stringable;

use function assert;

/**
 * @phpstan-ignore trait.unused
 */
trait FlashMessageControlHelperTrait
{
    /** All available control name constants */
    public const string CONTROL_FLASH_MESSAGE = 'flashMessage';

    /**
     * Component by name getter
     *
     * @param bool $throw Throw exception if component doesn't exist?
     * @return ComponentInterface|null
     */
    abstract public function getComponent(string $name, bool $throw = true): ?ComponentInterface;

    /**
     * FlashMessage control getter
     *
     * @param string $name [OPTIONAL]
     * @return FlashMessageControl
     */
    public function getComponentFlashMessage(string $name = self::CONTROL_FLASH_MESSAGE): FlashMessageControl
    {
        $control = $this->getComponent($name);
        assert($control instanceof FlashMessageControl);
        return $control;
    }

    /**
     * Saves the message to template, that can be displayed after redirect
     *
     * @param string|stdClass|FlashMessage|Stringable|HtmlStringable|null $message [OPTIONAL]
     * @param string $type [OPTIONAL] One of FlashMessageControl::TYPE_* constant
     * @param mixed[] $data
     * @phpstan-return ($message is (string|FlashMessage|Stringable|HtmlStringable) ? FlashMessage : stdClass)
     */
    public function flashMessage($message = null, string $type = FlashMessage::TYPE_INFO, array $data = []): stdClass
    {
        $flashMessageControl = $this->getComponentFlashMessage();
        $flashMessage = $flashMessageControl->flashMessage($message, $type, $data);
        return $flashMessage;
    }

    /**
     * Saves the error message to template, that can be displayed after redirect
     *
     * @param string $message
     * @param mixed[] $data [OPTIONAL]
     * @phpstan-return ($message is (string|FlashMessage|Stringable|HtmlStringable) ? FlashMessage : stdClass)
     */
    public function flashMessageError(string $message, array $data = []): FlashMessage|stdClass
    {
        $flashMessage = $this->flashMessage($message, FlashMessage::TYPE_ERROR, $data);
        return $flashMessage;
    }

    /**
     * Saves the info message to template, that can be displayed after redirect
     *
     * @param string $message
     * @param mixed[] $data [OPTIONAL]
     * @phpstan-return ($message is (string|FlashMessage|Stringable|HtmlStringable) ? FlashMessage : stdClass)
     */
    public function flashMessageInfo(string $message, array $data = []): FlashMessage|stdClass
    {
        $flashMessage = $this->flashMessage($message, FlashMessage::TYPE_INFO, $data);
        return $flashMessage;
    }

    /**
     * Saves the succes message to template, that can be displayed after redirect
     *
     * @param string $message
     * @param mixed[] $data [OPTIONAL]
     * @phpstan-return ($message is (string|FlashMessage|Stringable|HtmlStringable) ? FlashMessage : stdClass)
     */
    public function flashMessageSuccess(string $message, array $data = []): FlashMessage|stdClass
    {
        $flashMessage = $this->flashMessage($message, FlashMessage::TYPE_SUCCESS, $data);
        return $flashMessage;
    }

    /**
     * Saves the warning message to template, that can be displayed after redirect
     *
     * @param string $message
     * @param mixed[] $data [OPTIONAL]
     * @phpstan-return ($message is (string|FlashMessage|Stringable|HtmlStringable) ? FlashMessage : stdClass)
     */
    public function flashMessageWarning(string $message, array $data = []): FlashMessage|stdClass
    {
        $flashMessage = $this->flashMessage($message, FlashMessage::TYPE_WARNING, $data);
        return $flashMessage;
    }
}
