<?php

declare(strict_types=1);

namespace Interitty;

use Interitty\Application\UI\Control;
use Interitty\Bootstrap\Configurator;
use Interitty\FlashMessageControl\FlashMessageControl;
use Interitty\FlashMessageControl\FlashMessageControlHelperInterface;
use Interitty\FlashMessageControl\FlashMessageControlHelperTrait;
use Interitty\PhpUnit\BaseIntegrationTestCase as InterittyIntegrationTestCase;
use Interitty\Utils\FileSystem;
use Interitty\Utils\Strings;
use Nette\DI\Container;
use Nette\PhpGenerator\Helpers;

use function file_exists;

abstract class BaseIntegrationTestCase extends InterittyIntegrationTestCase
{
    /** All available class / interface / namespace name constants */
    protected const string NAME_NAMESPACE = 'Vendor\\Namespace';
    protected const string NAME_PRESENTER_CLASS = self::NAME_NAMESPACE . '\\TestPresenter';
    protected const string NAME_CONTROL_CLASS = self::NAME_NAMESPACE . '\\Control';

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * DI configurator factory
     *
     * <code>testName</code> parameter is for creating new container each test
     * @param string $configFilePath [OPTIONAL]
     * @return Configurator
     */
    protected function createConfigurator(string $configFilePath = ''): Configurator
    {
        $resetConfigContent = '
application:
    scanDirs: false

services:
    cacheStorage:
        class: Nette\Caching\Storages\MemoryStorage';

        $configurator = new Configurator();
        $configurator->addStaticParameters([
            'appDir' => ($appDir = $this->createTempDirectory('App')),
            'testName' => $this->getName(),
        ]);
        $configurator->setTempDirectory($this->createTempDirectory());
        $configurator->addConfig($this->createTempFile($resetConfigContent, 'nette-reset.neon'));
        if (file_exists($configFilePath) === true) {
            $configurator->addConfig($configFilePath);
        }
        // Ensure that deafult controls directory exist
        FileSystem::createDir($appDir . '/Controls');
        return $configurator;
    }

    /**
     * DI container factory
     *
     * @param string $configFilePath [OPTIONAL]
     * @return Container
     */
    protected function createContainer(string $configFilePath = ''): Container
    {
        $container = parent::createContainer($configFilePath);
        return $container;
    }

    /**
     * Control class generator
     *
     * @param string $className
     * @return void
     */
    protected function generateControl(string $className): void
    {
        $classShortName = Helpers::extractShortName($className);
        $namespace = Strings::before($className, '\\' . $classShortName, -1);
        $code = '<?php
declare(strict_types=1);

' . ((string) $namespace === '' ? '' : 'namespace ' . $namespace . ';') . '

use ' . Control::class . ' as BaseControl;
use ' . FlashMessageControl::class . ';
use ' . FlashMessageControlHelperInterface::class . ';
use ' . FlashMessageControlHelperTrait::class . ';

class ' . $classShortName . ' extends BaseControl implements FlashMessageControlHelperInterface
{
    use FlashMessageControlHelperTrait;
}';
        $this->processRegisterAutoload($className, $code);
    }

    // </editor-fold>
}
