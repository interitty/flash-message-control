<?php

declare(strict_types=1);

namespace Interitty\FlashMessageControl;

use Interitty\Application\UI\Presenter;
use Interitty\BaseIntegrationTestCase;
use PHPUnit\Framework\MockObject\MockObject;

use function assert;

/**
 * @coversDefaultClass Interitty\FlashMessageControl\FlashMessageControlHelperTrait
 */
class FlashMessageControlHelperTraitTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of flashMessage control getter implementation
     *
     * @return void
     * @covers ::getComponentFlashMessage
     * @group integration
     */
    public function testGetComponentFlashMessage(): void
    {
        $flashMessageControl = $this->createFlashMessageControlMock();
        $name = FlashMessageControlHelperInterface::CONTROL_FLASH_MESSAGE;
        $trait = $this->createTraitMock(['createComponent']);
        $trait->expects(self::once()) // @phpstan-ignore phpunit.mockMethod
            ->method('createComponent') // Should be getComponent, but it is `final`
            ->with(self::equalTo($name))
            ->willReturn($flashMessageControl);
        self::assertSame($flashMessageControl, $trait->getComponentFlashMessage($name));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of flash mesage handler implementation
     *
     * @return void
     * @covers ::flashMessage
     */
    public function testFlashMessage(): void
    {
        $message = 'message';
        $type = 'type';
        $data = ['foo' => 'foo'];
        $flashMessage = $this->createFlashMessageMock();
        $flashMessageControl = $this->createFlashMessageControlMock(['flashMessage']);
        $flashMessageControl->expects(self::once())
            ->method('flashMessage')
            ->with(self::equalTo($message), self::equalTo($type), self::equalTo($data))
            ->willReturn($flashMessage);
        $trait = $this->createTraitMock(['getComponentFlashMessage']);
        $trait->expects(self::once())
            ->method('getComponentFlashMessage')
            ->willReturn($flashMessageControl);
        self::assertSame($flashMessage, $trait->flashMessage($message, $type, $data));
    }

    /**
     * Tester of error flash mesage handler implementation
     *
     * @return void
     * @covers ::flashMessageError
     */
    public function testFlashMessageError(): void
    {
        $message = 'message';
        $data = ['foo' => 'foo'];
        $flashMessage = $this->createFlashMessageMock();
        $trait = $this->createTraitMock(['flashMessage']);
        $trait->expects(self::once())->method('flashMessage')
            ->with(self::equalTo($message), self::equalTo(FlashMessage::TYPE_ERROR), $data)
            ->willReturn($flashMessage);
        self::assertSame($flashMessage, $trait->flashMessageError($message, $data));
    }

    /**
     * Tester of info flash mesage handler implementation
     *
     * @return void
     * @covers ::flashMessageInfo
     */
    public function testFlashMessageInfo(): void
    {
        $message = 'message';
        $data = ['foo' => 'foo'];
        $flashMessage = $this->createFlashMessageMock();
        $trait = $this->createTraitMock(['flashMessage']);
        $trait->expects(self::once())->method('flashMessage')
            ->with(self::equalTo($message), self::equalTo(FlashMessage::TYPE_INFO), $data)
            ->willReturn($flashMessage);
        self::assertSame($flashMessage, $trait->flashMessageInfo($message, $data));
    }

    /**
     * Tester of success flash mesage handler implementation
     *
     * @return void
     * @covers ::flashMessageSuccess
     */
    public function testFlashMessageSuccess(): void
    {
        $message = 'message';
        $data = ['foo' => 'foo'];
        $flashMessage = $this->createFlashMessageMock();
        $trait = $this->createTraitMock(['flashMessage']);
        $trait->expects(self::once())->method('flashMessage')
            ->with(self::equalTo($message), self::equalTo(FlashMessage::TYPE_SUCCESS), $data)
            ->willReturn($flashMessage);
        self::assertSame($flashMessage, $trait->flashMessageSuccess($message, $data));
    }

    /**
     * Tester of warning flash mesage handler implementation
     *
     * @return void
     * @covers ::flashMessageWarning
     */
    public function testFlashMessageWarning(): void
    {
        $message = 'message';
        $data = ['foo' => 'foo'];
        $flashMessage = $this->createFlashMessageMock();
        $trait = $this->createTraitMock(['flashMessage']);
        $trait->expects(self::once())->method('flashMessage')
            ->with(self::equalTo($message), self::equalTo(FlashMessage::TYPE_WARNING), $data)
            ->willReturn($flashMessage);
        self::assertSame($flashMessage, $trait->flashMessageWarning($message, $data));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FlashMessage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FlashMessage&MockObject
     */
    protected function createFlashMessageMock(array $methods = []): FlashMessage&MockObject
    {
        $mock = $this->createPartialMock(FlashMessage::class, $methods);
        return $mock;
    }

    /**
     * FlashMessageControl mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FlashMessageControl&MockObject
     */
    protected function createFlashMessageControlMock(array $methods = []): FlashMessageControl&MockObject
    {
        $mock = $this->createPartialMock(FlashMessageControl::class, $methods);
        return $mock;
    }

    /**
     * Presenter mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @param string[] $addMethods [OPTIONAL] List of mocked method names
     * @return Presenter&MockObject
     */
    protected function createPresenterMock(array $methods = [], array $addMethods = []): Presenter&MockObject
    {
        $mock = $this->createMockAbstract(Presenter::class, $methods, $addMethods);
        return $mock;
    }

    /**
     * Control that use FlashMessageControlHelperTrait mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FlashMessageControlHelperInterface&MockObject
     */
    protected function createTraitMock(array $methods = []): FlashMessageControlHelperInterface&MockObject
    {
        /**
         * @phpstan-var class-string $traitClass
         * @phpstan-ignore varTag.nativeType
         */
        $traitClass = self::NAME_CONTROL_CLASS;
        $this->generateControl($traitClass);
        $mock = $this->createPartialMock($traitClass, $methods);
        assert($mock instanceof FlashMessageControlHelperInterface);
        return $mock;
    }

    // </editor-fold>
}
