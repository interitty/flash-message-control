<?php

declare(strict_types=1);

namespace Interitty\FlashMessageControl;

use Interitty\Application\UI\Presenter;
use Interitty\BaseIntegrationTestCase;
use Latte\Engine as LatteEngine;
use Nette\Bridges\ApplicationLatte\DefaultTemplate;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Localization\Translator as TranslatorInterface;
use PHPUnit\Framework\MockObject\MockObject;

use function array_merge;

/**
 * @coversDefaultClass Interitty\FlashMessageControl\FlashMessageControl
 */
class FlashMessageControlTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of flash message handler implementation
     *
     * @return void
     * @covers ::flashMessage
     * @group integration
     */
    public function testFlashMessage(): void
    {
        $message = 'message';
        $type = 'type';
        $data = ['foo' => 'foo'];
        $givenMessage = $this->createFlashMessageMock(['getMessage']);
        $givenMessage->expects(self::once())->method('getMessage')->willReturn($message);
        $resultMessage = $this->createFlashMessageMock();
        $flashMessageControl = $this->createFlashMessageControlMock(['createFlashMessage', 'redrawControl']);
        $flashMessageControl->expects(self::once())
            ->method('createFlashMessage')
            ->with(self::equalTo($message), self::equalTo($type), self::equalTo($data))
            ->willReturn($resultMessage);
        $flashMessageControl->expects(self::once())
            ->method('redrawControl')
            ->with(self::equalTo(FlashMessageControl::SNIPPET_FLASHES));

        self::assertSame($resultMessage, $flashMessageControl->flashMessage($givenMessage, $type, $data));
    }

    /**
     * Tester of flash message handler for string handle implementation
     *
     * @return void
     * @covers ::flashMessage
     * @group integration
     */
    public function testFlashMessageString(): void
    {
        $message = 'message';
        $type = 'type';
        $data = ['foo' => 'foo'];
        $resultMessage = $this->createFlashMessageMock();
        $flashMessageControl = $this->createFlashMessageControlMock(['createFlashMessage', 'redrawControl']);
        $flashMessageControl->expects(self::once())
            ->method('createFlashMessage')
            ->with(self::equalTo($message), self::equalTo($type), self::equalTo($data))
            ->willReturn($resultMessage);
        $flashMessageControl->expects(self::once())
            ->method('redrawControl')
            ->with(self::equalTo(FlashMessageControl::SNIPPET_FLASHES));

        self::assertSame($resultMessage, $flashMessageControl->flashMessage($message, $type, $data));
    }

    /**
     * Tester of flash message handler for object handle implementation
     *
     * @return void
     * @covers ::flashMessage
     * @group integration
     */
    public function testFlashMessageObject(): void
    {
        $message = (object) ['message' => 'message'];
        $type = 'type';
        $data = ['foo' => 'foo'];
        $flashMessageControl = $this->createFlashMessageControlMock(['createFlashMessage', 'redrawControl']);
        $flashMessageControl->expects(self::never())->method('createFlashMessage');
        $flashMessageControl->expects(self::once())
            ->method('redrawControl')
            ->with(self::equalTo(FlashMessageControl::SNIPPET_FLASHES));

        self::assertSame($message, $flashMessageControl->flashMessage($message, $type, $data));
    }

    /**
     * Tester of flash message factory implementation
     *
     * @return void
     * @covers ::createFlashMessage
     * @group integration
     */
    public function testCreateFlashMessageNoTranslator(): void
    {
        $message = 'message';
        $type = 'type';
        $data = ['foo' => 'foo'];
        $flashMessageControl = $this->createFlashMessageControlMock();
        /** @var FlashMessage $first */
        $first = $this->callNonPublicMethod($flashMessageControl, 'createFlashMessage', [$message, $type, $data]);
        /** @var FlashMessage $second */
        $second = $this->callNonPublicMethod($flashMessageControl, 'createFlashMessage', [$message, $type, $data]);

        self::assertNotSame($first, $second);
        self::assertSame($message, $first->getMessage());
        self::assertSame($type, $first->getType());
        self::assertSame($data, $first->getData());
        self::assertNull($first->getTranslator());
        self::assertSame($message, $second->getMessage());
        self::assertSame($type, $second->getType());
        self::assertSame($data, $second->getData());
        self::assertNull($second->getTranslator());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FlashMessage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FlashMessage&MockObject
     */
    protected function createFlashMessageMock(array $methods = []): FlashMessage&MockObject
    {
        $mock = $this->createPartialMock(FlashMessage::class, $methods);
        return $mock;
    }

    /**
     * FlashMessageControl mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FlashMessageControl&MockObject
     */
    protected function createFlashMessageControlMock(array $methods = []): FlashMessageControl&MockObject
    {
        $template = $this->createTemplate();
        $mockMethods = array_merge($methods, ['createTemplate']);
        $mock = $this->createPartialMock(FlashMessageControl::class, $mockMethods);
        $mock->expects(self::any())->method('createTemplate')->willReturn($template);
        $this->createPresenterMock()->addComponent($mock, 'flash');
        return $mock;
    }

    /**
     * Presenter mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Presenter&MockObject
     */
    protected function createPresenterMock(array $methods = []): Presenter&MockObject
    {
        $sessionSection = $this->createSessionSectionMock();
        $mockMethods = array_merge($methods, ['getFlashSession']);
        $mock = $this->createMockAbstract(Presenter::class, $mockMethods);
        $mock->expects(self::any())->method('getFlashSession')->willReturn($sessionSection);
        return $mock;
    }

    /**
     * Session mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Session&MockObject
     */
    protected function createSessionMock(array $methods = []): Session&MockObject
    {
        $mockMethods = array_merge($methods, ['autoStart']);
        $mock = $this->createPartialMock(Session::class, $mockMethods);
        return $mock;
    }

    /**
     * SessionSection  factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return SessionSection&MockObject
     */
    protected function createSessionSectionMock(array $methods = []): SessionSection&MockObject
    {
        $session = $this->createSessionMock();
        $mock = $this->createPartialMock(SessionSection::class, $methods);
        $mock->__construct($session, 'name');
        return $mock;
    }

    /**
     * Template factory
     *
     * @return DefaultTemplate
     */
    protected function createTemplate(): DefaultTemplate
    {
        $latte = $this->createLatteEngineMock();
        $template = new DefaultTemplate($latte);
        return $template;
    }

    /**
     * LatteEngine mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return LatteEngine&MockObject
     */
    protected function createLatteEngineMock(array $methods = []): LatteEngine&MockObject
    {
        $mock = $this->createPartialMock(LatteEngine::class, $methods);
        return $mock;
    }

    /**
     * Translator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TranslatorInterface&MockObject
     */
    protected function createTranslatorMock(array $methods = []): TranslatorInterface&MockObject
    {
        $mock = $this->createMockAbstract(TranslatorInterface::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
