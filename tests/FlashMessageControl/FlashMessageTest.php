<?php

declare(strict_types=1);

namespace Interitty\FlashMessageControl;

use Interitty\PhpUnit\BaseTestCase;
use Nette\Localization\Translator as TranslatorInterface;
use Nette\MemberAccessException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\FlashMessageControl\FlashMessage
 */
class FlashMessageTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of Constructor implementation
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $message = 'message';
        $type = 'type';
        $data = ['foo' => 'foo'];
        $flashMessage = $this->createFlashMessageMock(['setMessage', 'setType', 'setData']);
        $flashMessage->expects(self::once())->method('setMessage')->with(self::equalTo($message));
        $flashMessage->expects(self::once())->method('setType')->with(self::equalTo($type));
        $flashMessage->expects(self::once())->method('setData')->with(self::equalTo($data));

        $flashMessage->__construct($message, $type, $data);
    }

    /**
     * Tester of message property access helper implementation
     *
     * @return void
     * @covers ::__get
     */
    public function testPropertyGetMessage(): void
    {
        $message = 'message';
        $flashMessage = $this->createFlashMessageMock(['toString']);
        $flashMessage->expects(self::once())->method('toString')->willReturn($message);

        self::assertSame($message, $flashMessage->message);
    }

    /**
     * Tester of type property access helper implementation
     *
     * @return void
     * @covers ::__get
     */
    public function testPropertyGetType(): void
    {
        $type = 'type';
        $flashMessage = $this->createFlashMessageMock(['getType']);
        $flashMessage->expects(self::once())->method('getType')->willReturn($type);

        self::assertSame($type, $flashMessage->type);
    }

    /**
     * Tester of undeclared property access helper implementation
     *
     * @return void
     * @covers ::__get
     * @group negative
     */
    public function testPropertyGetUndeclared(): void
    {
        $flashMessage = $this->createFlashMessageMock();

        $this->expectException(MemberAccessException::class);
        $this->expectExceptionMessage('Cannot read an undeclared property :class:::name');
        $this->expectExceptionData([
            'class' => FlashMessage::class,
            'name' => 'undeclared',
        ]);
        $flashMessage->undeclared; // @phpstan-ignore expr.resultUnused
    }

    /**
     * Tester of serialization helper implementation
     *
     * @return void
     * @covers ::__serialize
     */
    public function testSerialize(): void
    {
        $message = 'message';
        $type = 'type';
        $expected = [
            'message' => $message,
            'type' => $type,
        ];
        $flashMessage = $this->createFlashMessageMock(['toString', 'getType']);
        $flashMessage->expects(self::once())->method('toString')->willReturn($message);
        $flashMessage->expects(self::once())->method('getType')->willReturn($type);

        self::assertSame($expected, $flashMessage->__serialize());
    }

    /**
     * Tester of convert flash message to string helper implementation
     *
     * @return void
     * @covers ::toString
     */
    public function testToString(): void
    {
        $message = 'message';
        $flashMessage = $this->createFlashMessageMock(['__toString']);
        $flashMessage->expects(self::once())->method('__toString')->willReturn($message);

        self::assertSame($message, $flashMessage->toString());
    }

    /**
     * Tester of convert exception to string magic method without translator implementation
     *
     * @return void
     * @covers ::__toString
     */
    public function testToStringNoTranslator(): void
    {
        $data = ['foo' => 'replaced foo'];
        $message = 'message :foo';
        $expectedMessage = 'message replaced foo';
        $flashMessage = $this->createFlashMessageMock(['getData', 'getMessage', 'getTranslator']);
        $flashMessage->expects(self::once())->method('getData')->willReturn($data);
        $flashMessage->expects(self::once())->method('getMessage')->willReturn($message);
        $flashMessage->expects(self::once())->method('getTranslator')->willReturn(null);

        self::assertSame($expectedMessage, $flashMessage->__toString());
    }

    /**
     * Tester of convert exception to string magic method with translator  implementation
     *
     * @return void
     * @covers ::__toString
     */
    public function testToStringTranslator(): void
    {
        $data = ['foo' => 'replaced foo'];
        $message = 'message :foo';
        $translatedMessage = 'translatedMessage :foo';
        $expectedMessage = 'translatedMessage replaced foo';
        $translator = $this->createTranslatorMock(['translate']);
        $translator->expects(self::once())
            ->method('translate')
            ->with(self::equalTo($message), self::equalTo($data))
            ->willReturn($translatedMessage);
        $flashMessage = $this->createFlashMessageMock(['getData', 'getMessage', 'getTranslator']);
        $flashMessage->expects(self::once())->method('getData')->willReturn($data);
        $flashMessage->expects(self::once())->method('getMessage')->willReturn($message);
        $flashMessage->expects(self::once())->method('getTranslator')->willReturn($translator);

        self::assertSame($expectedMessage, $flashMessage->__toString());
    }

    /**
     * Tester of data adder/getter/setter implementation
     *
     * @return void
     * @covers ::addData
     * @covers ::getData
     * @covers ::setData
     */
    public function testAddGetSetData(): void
    {
        $data = [
            'foo' => 'foo',
            'bar' => 'bar',
        ];
        $flashMessage = $this->createFlashMessageMock();
        self::assertCount(0, $flashMessage->getData());
        self::assertSame($flashMessage, $this->callNonPublicMethod($flashMessage, 'addData', ['baz', 'baz']));
        self::assertCount(1, $this->callNonPublicMethod($flashMessage, 'getData'));
        self::assertSame(['baz' => 'baz'], $this->callNonPublicMethod($flashMessage, 'getData'));
        self::assertSame($flashMessage, $this->callNonPublicMethod($flashMessage, 'setData', [[]]));
        self::assertCount(0, $this->callNonPublicMethod($flashMessage, 'getData'));
        self::assertSame($flashMessage, $this->callNonPublicMethod($flashMessage, 'setData', [$data]));
        self::assertCount(2, $this->callNonPublicMethod($flashMessage, 'getData'));
        self::assertSame($data, $this->callNonPublicMethod($flashMessage, 'getData'));
    }

    /**
     * Tester of message getter/setter implementation
     *
     * @return void
     * @covers ::getMessage
     * @covers ::setMessage
     */
    public function testGetSetMessage(): void
    {
        $message = 'message';
        $this->processTestGetSet(FlashMessage::class, 'message', $message);
    }

    /**
     * Tester of translator getter/setter implementation
     *
     * @return void
     * @covers ::getTranslator
     * @covers ::setTranslator
     */
    public function testGetSetTranslator(): void
    {
        $translator = $this->createTranslatorMock();
        $this->processTestGetSet(FlashMessage::class, 'translator', $translator);
    }

    /**
     * Tester of type getter/setter implementation
     *
     * @return void
     * @covers ::getType
     * @covers ::setType
     */
    public function testGetSetType(): void
    {
        $type = 'type';
        $this->processTestGetSet(FlashMessage::class, 'type', $type);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FlashMessage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FlashMessage&MockObject
     */
    protected function createFlashMessageMock(array $methods = []): FlashMessage&MockObject
    {
        $mock = $this->createPartialMock(FlashMessage::class, $methods);
        return $mock;
    }

    /**
     * Translator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TranslatorInterface&MockObject
     */
    protected function createTranslatorMock(array $methods = []): TranslatorInterface&MockObject
    {
        $mock = $this->createMockAbstract(TranslatorInterface::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
